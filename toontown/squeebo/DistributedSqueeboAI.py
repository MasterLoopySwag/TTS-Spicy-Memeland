# Distributed Squeebo AI
# Written by TrueBlueDogemon

from panda3d.core import *
from direct.directnotify import DirectNotifyGlobal
from toontown.toon import DistributedToonAI

class DistributedSqueeboAI:
    notify = DirectNotifyGlobal.directNotify.newCategory('DistributedSqueeboAI')
    
    def __init__(self):
        self.ownerId = 0
        self.nickname = "Squeebo"
        self.ability = "Unknown"
        self.heldItem = None
        self.maxHP = 10
        self.currHP = 10
        self.atkValue = 5
        self.defValue = 5
        self.spAtkValue = 5
        self.spDefValue = 5
        self.speedValue = 5
        self.hunger = 20
        self.happiness = 10
        return
        
    def getNickname(self):
        return self.nickname
        
    def setNickname(self, nickname):
        self.nickname = nickname
        
    def d_setNickname(self, nickname):
        self.sendUpdate('setNickname', [nickname])