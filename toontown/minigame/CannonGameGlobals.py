import random

TowerYRange = 200
GameTime = 180
MAX_SCORE = 100
MIN_SCORE = 15
FUSE_TIME = 0.0
CANNON_ROTATION_MIN = -20
CANNON_ROTATION_MAX = 90
CANNON_ROTATION_VEL = 25.0
CANNON_ANGLE_MIN = 0
CANNON_ANGLE_MAX = 360
CANNON_ANGLE_VEL = 25.0

def calcScore(t):
    range = MAX_SCORE - MIN_SCORE
    score = MAX_SCORE - range * (float(t) / GameTime)
    return int(score + 0.5)
