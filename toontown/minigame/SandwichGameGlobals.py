# Sandwich Stacker Globals

# This minigame is a brand new game made by Dogemon where you make sandwiches
# to fill the Toons' orders. Stack sandwiches quickly and correctly to earn BIG
# beans, but be sure not to drop them! Due to the nature of running a restaurant,
# this may be a LOT longer and more elaborate as anticipated. ;)

# imports go here



# Sandwich ingredients and other variable definitions
goodIngredients = ['salami', 'beef', 'chicken', 'cheddar', 'swiss',
                   'mozzarella', 'lettuce', 'tomato', 'pickles',
                   'ketchup', 'mustard', 'mayonnaise']
badIngredients = ['boot', 'stinky cheese', 'fish', 'anvil']