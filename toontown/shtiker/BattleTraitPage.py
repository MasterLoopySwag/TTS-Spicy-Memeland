import ShtikerPage
from toontown.battle import BattleTraitGlobals
from toontown.toonbase import TTLocalizer
from direct.gui.DirectGui import *
from panda3d.core import *

class BattleTraitPage(ShtikerPage.ShtikerPage):
    
    def __init__(self):
        ShtikerPage.ShtikerPage.__init__(self)
    
    def load(self):
        ShtikerPage.ShtikerPage.load(self)
        self.shieldImage = OnscreenImage(image="phase_3.5/maps/trait-shield.png", scale=1.0, parent=self)
        self.shieldImage.setTransparency(TransparencyAttrib.MAlpha)
        self.shieldImage.setScale(0.5)
        self.title = DirectLabel(parent=self, relief=None, text=TTLocalizer.BattleTraitPageTitle, text_scale=0.15, textMayChange=1, pos=(0, 0, 0.5))
        self.battleTraitID = DirectLabel(parent=self, relief=None, text=base.localAvatar.getBattleTrait(), text_scale=0.25, textMayChange=1, pos=(0, 0, 0))
        self.battleTraitDesc = DirectLabel(parent=self, relief=None, text=BattleTraitGlobals.BattleTraitDescriptions.get(base.localAvatar.getBattleTrait(), "No effect."), text_scale=0.1, textMayChange=1, pos=(0, 0, -0.3), text_wordwrap=14)
        self.memeBuckCount = DirectLabel(parent=self, relief=None, text="Meme Bucks: " + str(base.localAvatar.getMemeBucks()), text_scale=0.1, textMayChange=1, pos=(0, 0, -0.65))
        #To be finished at a later date. - Dogemon
        
    def _changeBattleTrait(self, newTrait):
        self.battleTraitID['text'] = newTrait
        self.battleTraitDesc['text'] = BattleTraitGlobals.BattleTraitDescriptions.get(newTrait, "No effect.")
        self.memeBuckCount['text'] = "Meme Bucks: " + str(base.localAvatar.getMemeBucks())
        
    def unload(self):
        self.ignoreAll()
        del self.title
        del self.battleTraitID
        del self.shieldImage
        del self.battleTraitDesc
        del self.memeBuckCount
        ShtikerPage.ShtikerPage.unload(self)
        
    def enter(self):
        ShtikerPage.ShtikerPage.enter(self)
        self.makePageBlue(None)
        self._changeBattleTrait(base.localAvatar.getBattleTrait())
        
    def exit(self):
        ShtikerPage.ShtikerPage.exit(self)
        self.makePageWhite(None)