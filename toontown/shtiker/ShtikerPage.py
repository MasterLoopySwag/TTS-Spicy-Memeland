import ShtikerBook
from direct.fsm import StateData
from direct.gui.DirectGui import *
from panda3d.core import *

class ShtikerPage(DirectFrame, StateData.StateData):

    def __init__(self):
        DirectFrame.__init__(self, relief=None, sortOrder=DGG.BACKGROUND_SORT_INDEX)
        self.initialiseoptions(ShtikerPage)
        StateData.StateData.__init__(self, 'shtiker-page-done')
        self.book = None
        self.hide()
        return

    def load(self):
        pass

    def unload(self):
        self.ignoreAll()
        del self.book

    def enter(self):
        self.show()

    def exit(self):
        self.hide()

    def setBook(self, book):
        self.book = book

    def setPageName(self, pageName):
        self.pageName = pageName

    def makePageWhite(self, item):
        white = Vec4(1, 1, 1, 1)
        self.book['image_color'] = white
        self.book.nextArrow['image_color'] = white
        self.book.prevArrow['image_color'] = white
        
    def makePageGray(self, item):
        gray = Vec4(0.5, 0.5, 0.5, 1)
        self.book['image_color'] = gray
        self.book.nextArrow['image_color'] = gray
        self.book.prevArrow['image_color'] = gray

    def makePageRed(self, item):
        red = Vec4(0.8, 0.35, 0.35, 1)
        self.book['image_color'] = red
        self.book.nextArrow['image_color'] = red
        self.book.prevArrow['image_color'] = red

    def makePageBlue(self, item):
        blue = Vec4(0, 0.6, 0.8, 1)
        self.book['image_color'] = blue
        self.book.nextArrow['image_color'] = blue
        self.book.prevArrow['image_color'] = blue
        
    def makePageYellow(self, item):
        yellow = Vec4(1, 1, 0.3, 1)
        self.book['image_color'] = yellow
        self.book.nextArrow['image_color'] = yellow
        self.book.prevArrow['image_color'] = yellow
