from toontown.safezone import SafeZoneLoader
from toontown.safezone import TTPlayground

class TTSafeZoneLoader(SafeZoneLoader.SafeZoneLoader):
    def __init__(self, hood, parentFSM, doneEvent):
        SafeZoneLoader.SafeZoneLoader.__init__(self, hood, parentFSM, doneEvent)
        self.playgroundClass = TTPlayground.TTPlayground
        self.musicFile = 'phase_4/audio/bgm/TC_nbrhood.ogg'
        self.activityMusicFile = 'phase_3.5/audio/bgm/TC_SZ_activity.ogg'
        self.dnaFile = 'phase_4/dna/toontown_central_sz.pdna'
        self.safeZoneStorageDNAFile = 'phase_4/dna/storage_TT_sz.pdna'

    def load(self):
        SafeZoneLoader.SafeZoneLoader.load(self)
        self.birdSound = map(base.loadSfx, ['phase_4/audio/sfx/SZ_TC_bird1.ogg',
                                            'phase_4/audio/sfx/SZ_TC_bird2.ogg',
                                            'phase_4/audio/sfx/SZ_TC_bird3.ogg'])
        bank = self.geom.find('**/*toon_landmark_TT_bank_DNARoot')
        library = self.geom.find('**/library/square_drop_shadow')
        doorTrigger = bank.find('**/door_trigger*')
        doorTrigger.setY(doorTrigger.getY() - 1.5)
        library.find('**/building_front').setY(0.3)
        library.find('**/front_entrance_flag').setY(0.1)
        self.dogemonSexyHead = loader.loadModel('phase_3/models/char/tt_a_chr_dgm_skirt_head_1000.bam')
        self.dogemonSexyHead.reparentTo(self.geom)
        self.dogemonSexyHead.find('**/head').setColor(0, 0.6, 0.8)
        self.dogemonSexyHead.find('**/head-front').setColor(0, 0.6, 0.8)
        self.dogemonSexyHead.setPos(113.8, 1.5, 40)
        self.dogemonSexyHead.setH(90)
        self.dogemonSexyHead.setScale(10)

    def unload(self):
        SafeZoneLoader.SafeZoneLoader.unload(self)
        self.dogemonSexyHead.removeNode()
        del self.dogemonSexyHead
        del self.birdSound
