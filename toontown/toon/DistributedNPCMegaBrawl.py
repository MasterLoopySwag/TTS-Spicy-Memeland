from otp.nametag.NametagConstants import CFSpeech, CFTimeout
from toontown.toonbase import TTLocalizer, ToontownGlobals
from toontown.toon import NPCToons
from DistributedNPCToonBase import DistributedNPCToonBase
import time

class DistributedNPCMegaBrawl(DistributedNPCToonBase):

    def __init__(self, cr):
        DistributedNPCToonBase.__init__(self, cr)
        self.lastCollision = 0
        self.megaBrawlDialog = None

    def disable(self):
        self.ignoreAll()
        self.destroyDialog()
        DistributedNPCToonBase.disable(self)

    def destroyDialog(self):
        self.clearChat()

        if self.megaBrawlDialog:
            self.megaBrawlDialog.destroy()
            self.megaBrawlDialog = None

    def getCollSphereRadius(self):
        return 2.5

    def handleCollisionSphereEnter(self, collEntry):
        if self.lastCollision > time.time():
            return

        self.lastCollision = time.time() + ToontownGlobals.NPCCollisionDelay

        base.cr.playGame.getPlace().fsm.request('stopped')
        base.setCellsAvailable(base.bottomCells, 0)
        self.setChatAbsolute(TTLocalizer.MegaBrawlUnimplemented, CFSpeech|CFTimeout)
        self.freeAvatar()
        return

    def freeAvatar(self):
        base.cr.playGame.getPlace().fsm.request('walk')
        base.setCellsAvailable(base.bottomCells, 1)