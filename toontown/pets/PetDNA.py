from toontown.toon import ToonDNA
from pandac.PandaModules import VBase4
from toontown.toonbase import TTLocalizer, ToontownGlobals
from direct.showbase import PythonUtil
NumFields = 10
Fields = {'head': 0,
 'ears': 1,
 'nose': 2,
 'tail': 3,
 'body': 4,
 'color': 5,
 'colorScale': 6,
 'eyes': 7,
 'gender': 8,
 'hat': 9}
HeadParts = ['feathers']
EarParts = ['horns',
 'antennae',
 'dogEars',
 'catEars',
 'rabbitEars']
EarTextures = {'horns': None,
 'antennae': None,
 'dogEars': None,
 'catEars': 'phase_4/maps/BeanCatEar6.jpg',
 'rabbitEars': 'phase_4/maps/BeanBunnyEar6.jpg'}
ExoticEarTextures = {'horns': None,
 'antennae': None,
 'dogEars': None,
 'catEars': 'phase_4/maps/BeanCatEar3Yellow.jpg',
 'rabbitEars': 'phase_4/maps/BeanBunnyEar6.jpg'}
NoseParts = ['clownNose',
 'dogNose',
 'ovalNose',
 'pigNose']
TailParts = ['catTail',
 'longTail',
 'birdTail',
 'bunnyTail']
TailTextures = {'catTail': 'phase_4/maps/beanCatTail6.jpg',
 'longTail': 'phase_4/maps/BeanLongTail6.jpg',
 'birdTail': None,
 'bunnyTail': None}
GiraffeTail = 'phase_4/maps/BeanLongTailGiraffe.jpg'
LeopardTail = 'phase_4/maps/BeanLongTailLepord.jpg'
GenericBodies = ['dots',
 'threeStripe',
 'tigerStripe',
 'tummy']
SpecificBodies = ['turtle', 'giraffe', 'leopard', 'jazz']
BodyTypes = GenericBodies + SpecificBodies
PetRarities2 = (('leopard', 0.005),
 ('giraffe', 0.015),
 ('turtle', 0.045),
 ('tigerStripe', 0.115),
 ('dots', 0.265),
 ('tummy', 0.525),
 ('threeStripe', 1.0),
 ('jazz', 0.025))
PetRarities = {'body': {ToontownGlobals.ToontownCentral: {'threeStripe': 50,
                                            'tummy': 30,
                                            'dots': 20,
                                            'jazz': 5},
          ToontownGlobals.DonaldsDock: {'threeStripe': 35,
                                        'tummy': 30,
                                        'dots': 20,
                                        'tigerStripe': 15,
                                        'jazz': 5},
          ToontownGlobals.DaisyGardens: {'threeStripe': 15,
                                         'tummy': 20,
                                         'dots': 20,
                                         'tigerStripe': 20,
                                         'turtle': 15,
                                         'jazz': 5},
          ToontownGlobals.MinniesMelodyland: {'threeStripe': 10,
                                              'tummy': 15,
                                              'dots': 30,
                                              'tigerStripe': 25,
                                              'turtle': 20,
                                              'jazz': 5},
          ToontownGlobals.TheBrrrgh: {'threeStripe': 5,
                                      'tummy': 10,
                                      'dots': 20,
                                      'tigerStripe': 25,
                                      'turtle': 25,
                                      'giraffe': 15,
                                      'jazz': 5},
          ToontownGlobals.DonaldsDreamland: {'threeStripe': 5,
                                             'tummy': 5,
                                             'dots': 15,
                                             'tigerStripe': 20,
                                             'turtle': 25,
                                             'giraffe': 20,
                                             'leopard': 10,
                                             'jazz': 5}
        }}
BodyTextures = {'dots': 'phase_4/maps/BeanbodyDots6.jpg',
 'threeStripe': 'phase_4/maps/Beanbody3stripes6.jpg',
 'tigerStripe': 'phase_4/maps/BeanbodyZebraStripes6.jpg',
 'turtle': 'phase_4/maps/BeanbodyTurtle.jpg',
 'giraffe': 'phase_4/maps/BeanbodyGiraffe1.jpg',
 'leopard': 'phase_4/maps/BeanbodyLepord2.jpg',
 'tummy': 'phase_4/maps/BeanbodyTummy6.jpg',
 'jazz': 'phase_4/maps/BeanbodyJazz.png'}
FeetTextures = {'normal': 'phase_4/maps/BeanFoot6.jpg',
 'turtle': 'phase_4/maps/BeanFootTurttle.jpg',
 'giraffe': 'phase_4/maps/BeanFootYellow3.jpg',
 'leopard': 'phase_4/maps/BeanFootYellow3.jpg'}

AllPetColors = (VBase4(1.0, 1.0, 1.0, 1.0), #White
                VBase4(1.0, 0.75, 0.75, 1.0), #Peach
                VBase4(1.0, 0.25, 0.25, 1.0), #Bright Red
                VBase4(0.8, 0.4, 0.4, 1.0), #Red
                VBase4(0.75, 0.25, 0.5, 1.0), #Maroon
                VBase4(0.5, 0.0, 0.0, 1.0), #Burgundy
                VBase4(0.5, 0.35, 0.15, 1.0), #Sienna
                VBase4(0.5, 0.25, 0.0, 1.0), #Brown
                VBase4(1.0, 0.7, 0.5, 1.0), #Tan
                VBase4(0.8, 0.5, 0.35, 1.0), #Coral
                VBase4(0.8, 0.25, 0.125, 1.0), #Burnt Orange
                VBase4(1.0, 0.5, 0.0, 1.0), #Orange
                VBase4(1.0, 0.85, 0.25, 1.0), #Yellow
                VBase4(1.0, 1.0, 0.5, 1.0), #Cream
                VBase4(0.85, 1.0, 0.5, 1.0), #Citrine
                VBase4(0.55, 0.8, 0.3, 1.0), #Lime
                VBase4(0.25, 0.75, 0.5, 1.0), #Sea Green
                VBase4(0.35, 1.0, 0.35, 1.0), #Green
                VBase4(0.0, 0.5, 0.0, 1.0), #Forest Green
                VBase4(0.6, 1.0, 0.6, 1.0), #Mint
                VBase4(0.45, 1.0, 0.75, 1.0), #Light Blue
                VBase4(0.35, 0.8, 1.0, 1.0), #Aqua
                VBase4(0.25, 0.75, 1.0, 1.0), #Turquoise
                VBase4(0.0, 0.6, 0.8, 1.0), #Blue
                VBase4(0.5, 0.6, 0.8, 1.0), #Periwinkle
                VBase4(0.3, 0.3, 0.75, 1.0), #Royal Blue
                VBase4(0.45, 0.35, 0.85, 1.0), #Slate Blue
                VBase4(0.0, 0.0, 0.5, 1.0), #Navy Blue
                VBase4(0.5, 0.3, 0.75, 1.0), #Purple
                VBase4(0.85, 0.5, 0.85, 1.0), #Lavender
                VBase4(1.0, 0.35, 0.6, 1.0), #Dank Pink
                VBase4(1.0, 0.6, 1.0, 1.0), #Pink
                VBase4(0.7, 0.7, 0.8, 1.0), #Plum
                VBase4(0.5, 0.5, 0.5, 1.0), #Gray
                VBase4(0.25, 0.25, 0.25, 1.0)) #Black / 34 colors (starts at zero)

GenericPetColors = [0,
 1,
 3,
 5,
 6,
 7,
 8,
 9,
 10,
 11,
 12,
 13,
 14,
 15,
 16,
 17,
 18,
 19,
 21,
 22,
 23,
 24,
 25,
 26,
 27,
 28,
 29,
 30,
 31,
 32,
 33,
 34]
ColorScales = [0.8,
 0.85,
 0.9,
 0.95,
 1.0,
 1.05,
 1.1,
 1.15,
 1.2]
PetEyeColors = (VBase4(1.0, 0.0, 0.0, 1.0), #Red
 VBase4(0.0, 1.0, 0.0, 1.0), #Green
 VBase4(0.0, 0.0, 1.0, 1.0), #Blue
 VBase4(1.0, 1.0, 0.0, 1.0), #Yellow
 VBase4(0.0, 1.0, 1.0, 1.0), #Cyan
 VBase4(1.0, 0.0, 1.0, 1.0), #Magenta
 VBase4(1.0, 0.5, 0.0, 1.0), #Orange
 VBase4(0.0, 1.0, 0.5, 1.0), #Teal
 VBase4(1.0, 0.0, 0.5, 1.0), #Purple
 VBase4(1.0, 0.5, 0.5, 1.0), #Pink
 VBase4(0.5, 1.0, 0.5, 1.0), #Mint
 VBase4(0.5, 0.25, 0.0, 1.0), #Brown
 VBase4(1.0, 1.0, 1.0, 1.0), #White
 VBase4(0.5, 0.5, 0.5, 1.0), #Gray
 VBase4(0.0, 0.0, 0.0, 1.0)) #Black
PetGenders = [0, 1]
PetHats = ToonDNA.HatModels
PetDragonWings = ['dragon'] #TODO: Fully implement Dragon wings.

def getRandomPetDNA(zoneId = ToontownGlobals.DonaldsDreamland):
    from random import choice
    head = choice(range(-1, len(HeadParts)))
    ears = choice(range(-1, len(EarParts)))
    nose = choice(range(-1, len(NoseParts)))
    tail = choice(range(-1, len(TailParts)))
    body = getSpecies(zoneId)
    color = choice(range(0, len(getColors(body))))
    colorScale = choice(range(0, len(ColorScales)))
    eyes = choice(range(0, len(PetEyeColors)))
    gender = choice(range(0, len(PetGenders)))
    hat = choice(range(-1, len(PetHats)))
    #wings = choice(range(-1, len(PetDragonWings)))
    return [head,
     ears,
     nose,
     tail,
     body,
     color,
     colorScale,
     eyes,
     gender,
     hat]


def getSpecies(zoneId):
    body = PythonUtil.weightedRand(PetRarities['body'][zoneId])
    return BodyTypes.index(body)


def getColors(bodyType):
    return GenericPetColors


def getFootTexture(bodyType):
    if BodyTypes[bodyType] == 'turtle':
        texName = FeetTextures['turtle']
    elif BodyTypes[bodyType] == 'giraffe':
        texName = FeetTextures['giraffe']
    elif BodyTypes[bodyType] == 'leopard':
        texName = FeetTextures['leopard']
    else:
        texName = FeetTextures['normal']
    return texName


def getEarTexture(bodyType, earType):
    if BodyTypes[bodyType] == 'giraffe' or BodyTypes[bodyType] == 'leopard':
        dict = ExoticEarTextures
    else:
        dict = EarTextures
    return dict[earType]


def getBodyRarity(bodyIndex):
    bodyName = BodyTypes[bodyIndex]
    totalWeight = 0.0
    weight = {}
    for zoneId in PetRarities['body']:
        for body in PetRarities['body'][zoneId]:
            totalWeight += PetRarities['body'][zoneId][body]
            if body in weight:
                weight[body] += PetRarities['body'][zoneId][body]
            else:
                weight[body] = PetRarities['body'][zoneId][body]

    minWeight = min(weight.values())
    rarity = (weight[bodyName] - minWeight) / (totalWeight - minWeight)
    return rarity


def getRarity(dna):
    body = dna[Fields['body']]
    rarity = getBodyRarity(body)
    return rarity


def getGender(dna):
    return dna[Fields['gender']]


def setGender(dna, gender):
    dna[Fields['gender']] = gender


def getGenderString(dna = None, gender = -1):
    if dna != None:
        gender = getGender(dna)
    if gender:
        return TTLocalizer.GenderShopBoyButtonText
    else:
        return TTLocalizer.GenderShopGirlButtonText
    return

def getPetHat(dna):
    return dna[Fields['hat']]

def setPetHat(dna, hat):
    dna[Fields['hat']] = hat
