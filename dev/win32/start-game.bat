@echo off

title Toontown Stride Game Launcher

echo Choose your connection method!
echo.
echo #1 - Local
echo #2 - Remote
echo.

:selection

set INPUT=-1
set /P INPUT=Selection: 

if %INPUT%==1 (
    set TTS_GAMESERVER=127.0.0.1
) else if %INPUT%==2 (
    echo.
    set /P TTS_GAMESERVER="Gameserver: "
) else (
    echo "You must choose an option!"
    goto selection )

echo.
echo.

set /P TTS_PLAYCOOKIE="Username: "

echo ===============================
echo Starting Toontown Stride...
echo ppython: "C:\Panda3D-1.10.0\python\ppython.exe"

echo Username: %TTS_PLAYCOOKIE%
echo Gameserver: %TTS_GAMESERVER%
echo ===============================

cd ../../

:main
"C:\Panda3D-1.10.0\python\ppython.exe" -m toontown.toonbase.ToontownStart

pause

goto main
