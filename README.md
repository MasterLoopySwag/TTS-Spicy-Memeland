# Welcome to Toontown Spicy Memeland!

Features include:
- An outrageous number of new fish and trophies
- Doodle Hats
- Rank 9 Cogs
- ALL Cogs in Catching Game and Maze Game
- Silly Skills (and the ability to earn them from tasks)
- Elite Cogs (WIP)
- Ultra Jellybean Holiday
- New Cog attacks: Green Missile and Glitch (WIPs of course)
- Some Cogs wear backpacks in an attempt to make them more intimidating
- Squeebo CE and exclusive Magic Word to tweak the eye and body colors thereof
- Sandwich Tank CE
- Quite a few new accessories and accessory textures
- and more!

# Contributions?
While this is a personal source, I do welcome any contributions to the source as long as you do like forks and pull requests. Try not to do too many changes all at once, though, because I don't like merge conflicts. (LOL)
